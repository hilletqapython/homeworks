###################### DECORATOR ######################

#
# def foo():
#     print("Function foo")
#
#
# def bar(func):
#     res = func()
#
#
# bar(foo)


def bar():
    def spam():
        print("Function spam")
    return spam

# bar()

res = bar()

res()