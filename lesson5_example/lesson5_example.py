##############################  Functions

# def my_function(arg1, arg2, arg3):
#     result = arg1 + arg2
#
#     return result
#
# res = my_function(arg1=3, arg3=2, arg2=6)
# print(res)


def my_function(limit, count):
    while True:
        count += 1
        if count > limit:
            return
        print(count)


my_function(5, 0)
