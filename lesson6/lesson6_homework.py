"""
Task

Write the program "Cashier in the cinema", which will do the following:

Ask the user to enter their own age.

- if the user is less than 7 - display "You <>! Where are your parents?"

- if the user is less than 16 - display "You only <>, and this is an adult film!"

- if the user is more than 65 - display "You <>? Show your retirement ID!"

- if the user's age consists of the same numbers (11, 22, 44, etc., all possible options!) -
  display "Oh, you <>! What an interesting age!"

- in any other case - withdraw "Despite the fact that you <>, there are still no tickets!"

Instead of <> in each answer, substitute the meaning of age (number) and the correct form
of the word year

For any answer, the form of the word "year" must match the meaning of the user's age.


After receiving the answer, the program should ask the user if he wants to repeat
the execution. The user responds to Yes or No.
In the case when the user has answered Yes, the program repeats, No. - ends.
The user must be able to repeat the program an unlimited number of times.
"""


def user_input():
    age = int(input("Enter your age: "))
    return age


def age_check(age):
    """
    Check if user's age consists of the same numbers
    :return (str) statement if numbers are same

    Check the age and return a right statement for that age
    :return (str) statement that answer to the age
    """
    if age > 10:
        age_str = str(age)  # converting (int) to (str) for checking first and second digit in age
        if age_str[0] == age_str[1]:
            print(f"Oh, you are {age} years old! What an interesting age!")
            return  # exit function if first and second digit same

    if age < 7:
        if age == 1:  # if user age is 1, then we'll write "year", in all other cases "years"
            return print(f"You are {age} year old! Where are your parents?")
        else:
            return print(f"You are {age} years old! Where are your parents?")
    elif age < 16:
        return print(f"You are only {age} years old, and this is an adult film!")
    elif age > 65:
        return print(f"You are {age} years old? Show your retirement ID!")
    else:
        return print(f"Despite the fact that you are {age} years old, there are still no tickets!")


def repeat_game():
    """
    Asking user if he wants to repeat the game
    :return: (bool) game_repeat
    """
    while True:
        repeat = input("Do you want to repeat? Yes or No: ").lower()
        if repeat == "no":
            print("Program finished")
            return False
        else:
            return game()


def game():
    entered_age = user_input()
    age_check(age=entered_age)
    repeat_game()


game()
