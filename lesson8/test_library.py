import pytest

import lesson8.library as library

choices_kopeika_correct_form = [
    ([0, 1], "копейка"),
    ([1, 5], "копеек"),
    ([10, 23], "копейки"),
]


@pytest.mark.parametrize("value, expected", choices_kopeika_correct_form)
def test_kopeika_correct_form(value, expected):
    assert library.kopeika_correct_form(value) == expected


choices_rubble_correct_form = [
    ([0, 1], "рублей"),
    ([1, 5], "рубль"),
    ([22, 23], "рубля"),
]


@pytest.mark.parametrize("value, expected", choices_rubble_correct_form)
def test_rubble_correct_form(value, expected):
    assert library.rubble_correct_form(value) == expected


is_hot_day_choices = [
    (12, False),
    (26, True),
]


@pytest.mark.parametrize("value, expected", is_hot_day_choices)
def test_is_hot_day(value, expected):
    assert library.is_hot_day(value) == expected


number_into_list_examples = [
    (5, ['5 рублей', '0 копеек']),
    (0.21, ['0 рублей', '21 копейка'])
]


@pytest.mark.parametrize("value, expected", number_into_list_examples)
def test_number_into_list(value, expected):
    assert library.number_into_list(value) == expected
