from typing import Union


def kopeika_correct_form(deposit_list: list) -> str:
    """
    Receiving number from a list and checkin what form of word "копейка" we need to use with it
    # 1 - копейка
    # 2, 3, 4 - копейки
    # 0, 5, 6, 7, 8, 9- копеек
    # 11, 12, 13, 14 - копеек
    :param deposit_list: (list) coming from "user_to_input_deposit" function
    :return: (str) right form of word "копейка"
    """
    deposit_list = int(deposit_list[1])
    lst1 = [2, 3, 4]                # 2, 3, 4 - копейки
    lst2 = [0, 5, 6, 7, 8, 9]       # 0, 5, 6, 7, 8, 9- копеек
    lst3 = [11, 12, 13, 14]         # 11, 12, 13, 14 - копеек  (everything else is копейка)
    if deposit_list in lst3 or deposit_list % 10 in lst2:
        result = "копеек"
    elif deposit_list % 10 in lst1:
        result = "копейки"
    else:
        result = "копейка"
    return result


def rubble_correct_form(deposit_list: list) -> str:
    """
    Receiving number from a list and checkin what form of word "рубль" we need to use with it
    # 1 - рубль
    # 2, 3, 4 - рубля
    # 0, 5, 6, 7, 8, 9- рублей
    # 11, 12, 13, 14 - рублей
    :param deposit_list: (list) coming from "user_to_input_deposit" function
    :return: (str) right form of word "рубль"
    """
    deposit_list = int(deposit_list[0])
    lst1 = [2, 3, 4]                  # 2, 3, 4 - рубля
    lst2 = [0, 5, 6, 7, 8, 9]         # 0, 5, 6, 7, 8, 9 - рублей
    lst3 = [11, 12, 13, 14]           # 11, 12, 13, 14 - рублей
    if deposit_list in lst3 or deposit_list % 10 in lst2:
        result = "рублей"
    elif deposit_list % 10 in lst1:
        result = "рубля"
    else:
        result = "рубль"
    return result


def number_into_list(value: Union[int, float]) -> list:
    """
    Receiving number from user, converting this number into list and adding right forms of "рубль" and "копейка" to it
    :param (int, float) value
    :return: (list) final_result_in_list with number and correct forms of "рубль" and "копейка" in  it
    """
    value_float = float(value)    # if entered value is int it will be converted
    final_result_in_list = []
    value_list = str(value_float).split(".")

    final_result_in_list.append(value_list[0] + " " + rubble_correct_form(value_list))    # [number+"копейка"]
    final_result_in_list.append(value_list[1] + " " + kopeika_correct_form(value_list))  # [number+"рубль"]
    return final_result_in_list


def is_hot_day(entered_temperature: int) -> bool:
    """
    :param  (int) entered_temperature
    :return: (bool) if => 25 - True, if < 25 -False
    """
    day_is_hot = True
    if entered_temperature < 25:
        day_is_hot = False
    return day_is_hot


def user_to_input_number() -> float:
    """
    Asking user to enter a number
    If input incorrect(got letters or spaces between numbers) asking user to re-enter
    :return: (float) entered number
    """
    condition = True
    while condition:
        result = input("Enter number:")

        # check if number is float.
        def check_if_input_is_float():
            try:
                float(result)
                return True
            except ValueError:
                return False

        is_result_float = check_if_input_is_float()
        if result.isdigit() or is_result_float == True:
            result = float(result)
            return result
        else:
            condition = True
            print("Incorrect input, please try again")

