import lesson8.library as library


def deposit_from_user() -> str:
    """
    Taking input from user.
    Checking if input is correct (int or float)
    If input is incorrect - asking for input one more time
    :return: (str) with entered deposit number and correct forms of "копейка" and "рубль" words in it
    """
    while True:
        deposit = input(f"Ender sum you want to deposit: ")

        # check if number is float.
        # That's the only way I know how to check for float.
        # please give me one simplest way if you know. Thank You!
        def check_if_input_is_float() -> bool:
            try:
                float(deposit)
                return True
            except ValueError:
                return False
        is_result_float = check_if_input_is_float()

        # if input is correct(must be float or int) proceed further, else return to input again
        if is_result_float or deposit.isdigit():

            # converting number in float and round it to 2 digits after decimal
            end_number_float = round(float(deposit), 2)

            # split float number by decimal point making it list of 2 numbers
            end_number_list = str(end_number_float).split(".")

            # receiving right form of word "копейка"
            kopeika_word_right_form = library.kopeika_correct_form(end_number_list)

            # receiving right form of word "рубль"
            rubble_word_right_form = library.rubble_correct_form(end_number_list)

            end_number_float = f"Cумма, которую вы предоставили: {end_number_list[0]}  {rubble_word_right_form} {end_number_list[1]} {kopeika_word_right_form} "
            return end_number_float


if __name__ == "__main__":
    print(deposit_from_user())
    print(library.user_to_input_number())



